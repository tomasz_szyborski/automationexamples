package com.billennium.cucumber.steps;

import com.billennium.cucumber.contextStorage.TestContext;

import static com.billennium.cucumber.contextStorage.TestContext.CONTEXT;

public abstract class AbstractSteps {

    public TestContext testContext() {
        return CONTEXT;
    }

}
