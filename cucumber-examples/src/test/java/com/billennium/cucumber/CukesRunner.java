package com.billennium.cucumber;

import com.billennium.cucumber.contextStorage.ContextNames;
import com.billennium.cucumber.contextStorage.TestContext;
import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;

import static com.billennium.cucumber.contextStorage.TestContext.testContext;

@CucumberOptions(features = "src/test/resources/features",
        plugin = {"pretty", "html:target/cucumber"},
        tags = "@accounting",
        glue = "classpath:com/billennium/cucumber/steps")
@Test
public class CukesRunner extends AbstractTestNGCucumberTests {

    @Before
    public void scenarioSetup() {
        testContext().set(ContextNames.ERROR_MESSAGE, "");
    }

    @After
    public void scenarioTeardown() {
        testContext().reset();
    }
}