package com.billennium.cucumber;

import com.billennium.cucumber.mocks.Account;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;

@CucumberOptions(features = "src/test/resources/features",
        plugin = {"pretty", "html:target/cucumber"},
        tags = {"@accounting" ,"not @wip"},
        glue = "classpath:com/billennium/cucumber/steps")
@Test
public class CukesRunner extends AbstractTestNGCucumberTests {

    @Before
    public void scenarioSetup() {

    }

    @After
    public void scenarioTeardown() {
    }
}