package com.billennium.cucumber.assertions;

import com.billennium.cucumber.mocks.Account;

public class CustomAssertions {

    public static AccountAssert assertThat(Account actual) {
        return new AccountAssert(actual);
    }
}
