package com.billennium.automationpractice.pageObjects.modules;

import com.billennium.automationpractice.pageObjects.BaseAbstraction;
import org.openqa.selenium.WebDriver;

public abstract class BaseModule extends BaseAbstraction {
    public BaseModule(WebDriver driver) {
        super(driver);
    }
}
