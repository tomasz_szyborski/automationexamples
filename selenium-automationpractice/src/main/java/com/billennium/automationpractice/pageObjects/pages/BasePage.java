package com.billennium.automationpractice.pageObjects.pages;

import com.billennium.automationpractice.pageObjects.BaseAbstraction;
import com.billennium.automationpractice.pageObjects.modules.BottomBarModule;
import com.billennium.automationpractice.pageObjects.modules.TopBarModule;
import com.billennium.automationpractice.utils.Client;
import org.openqa.selenium.WebDriver;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class BasePage extends BaseAbstraction {

    protected static final String baseURL = "http://automationpractice.com/";

    protected TopBarModule topBarModule;
    protected BottomBarModule bottomBarModule;

    public BasePage(WebDriver driver) {
        super(driver);
        topBarModule = new TopBarModule(driver);
        bottomBarModule = new BottomBarModule(driver);
    }

    public void openHomePage(){
        driver.get(baseURL);
    }

    public void checkIfUserIsLoggedIn(Client client){
        assertThat(topBarModule.getMyAccountButtonText())
                .as("My account has user name")
                .isEqualTo(client.getFirstName() + " " + client.getLastName());
    }

    public void checkIfUserIsNotLoggedIn(){
        topBarModule.checkMyAccountButtonAvailability();
    }

    public void logOut(){
        topBarModule.logOut();
    }

    public abstract void isAt();
}
